
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Jan 2021 14:04:21 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="description" content="Smarthr - Bootstrap Admin Template">
        <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
        <meta name="author" content="Dreamguys - Bootstrap Admin Template">
        <meta name="robots" content="noindex, nofollow">
        <title>Dashboard - HRMS admin template</title>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/favicon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

        <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://dreamguys.co.in/smarthr/blue/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://dreamguys.co.in/smarthr/blue/assets/css/line-awesome.min.css">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Lineawesome CSS -->
        <link rel="stylesheet" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/css/line-awesome.min.css">

        <!-- Chart CSS -->
        <link rel="stylesheet" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/plugins/morris/morris.css">

        <!-- Main CSS -->
        <link rel="stylesheet" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/css/style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

    </head>

    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            <div class="header">

                <!-- Logo -->
                <div class="header-left">
                    <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/index" class="logo">
                        <img src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/logo.png" width="40" height="40" alt="">
                    </a>
                </div>
                <!-- /Logo -->

                <a id="toggle_btn" href="javascript:void(0);">
                    <span class="bar-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </a>

                <!-- Header Title -->
                <div class="page-title-box">
                    <h3>Web Based Timetable</h3>
                </div>
                <!-- /Header Title -->

                <a id="mobile_btn" class="mobile_btn" href="#sidebar"><i class="fa fa-bars"></i></a>

                <!-- Header Menu -->
                <ul class="nav user-menu">

                    <!-- Search -->
                    <li class="nav-item">
                        <div class="top-nav-search">
                            <a href="javascript:void(0);" class="responsive-search">
                                <i class="fa fa-search"></i>
                            </a>
                            <form action="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/search">
                                <input class="form-control" type="text" placeholder="Search here">
                                <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </li>
                    <!-- /Search -->

                    <!-- Flag -->
                    <li class="nav-item dropdown has-arrow flag-nav">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                            <img src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/flags/us.png" alt="" height="20"> <span>English</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/flags/us.png" alt="" height="16"> English
                            </a>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/flags/fr.png" alt="" height="16"> French
                            </a>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/flags/es.png" alt="" height="16"> Spanish
                            </a>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/flags/de.png" alt="" height="16"> German
                            </a>
                        </div>
                    </li>
                    <!-- /Flag -->

                    <!-- Notifications -->
                    <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i> <span class="badge badge-pill">3</span>
                        </a>
                        <div class="dropdown-menu notifications">
                            <div class="topnav-dropdown-header">
                                <span class="notification-title">Notifications</span>
                                <a href="javascript:void(0)" class="clear-noti"> Clear All </a>
                            </div>
                            <div class="noti-content">
                                <ul class="notification-list">
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/activities">
                                            <div class="media">
                                                <span class="avatar">
                                                    <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-02.jpg">
                                                </span>
                                                <div class="media-body">
                                                    <p class="noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
                                                    <p class="noti-time"><span class="notification-time">4 mins ago</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/activities">
                                            <div class="media">
                                                <span class="avatar">
                                                    <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-03.jpg">
                                                </span>
                                                <div class="media-body">
                                                    <p class="noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
                                                    <p class="noti-time"><span class="notification-time">6 mins ago</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/activities">
                                            <div class="media">
                                                <span class="avatar">
                                                    <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-06.jpg">
                                                </span>
                                                <div class="media-body">
                                                    <p class="noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
                                                    <p class="noti-time"><span class="notification-time">8 mins ago</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/activities">
                                            <div class="media">
                                                <span class="avatar">
                                                    <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-17.jpg">
                                                </span>
                                                <div class="media-body">
                                                    <p class="noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
                                                    <p class="noti-time"><span class="notification-time">12 mins ago</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/activities">
                                            <div class="media">
                                                <span class="avatar">
                                                    <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-13.jpg">
                                                </span>
                                                <div class="media-body">
                                                    <p class="noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
                                                    <p class="noti-time"><span class="notification-time">2 days ago</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="topnav-dropdown-footer">
                                <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/activities">View all Notifications</a>
                            </div>
                        </div>
                    </li>
                    <!-- /Notifications -->

                    <!-- Message Notifications -->
                    <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                            <i class="fa fa-comment-o"></i> <span class="badge badge-pill">8</span>
                        </a>
                        <div class="dropdown-menu notifications">
                            <div class="topnav-dropdown-header">
                                <span class="notification-title">Messages</span>
                                <a href="javascript:void(0)" class="clear-noti"> Clear All </a>
                            </div>
                            <div class="noti-content">
                                <ul class="notification-list">
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/chat">
                                            <div class="list-item">
                                                <div class="list-left">
                                                    <span class="avatar">
                                                        <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-09.jpg">
                                                    </span>
                                                </div>
                                                <div class="list-body">
                                                    <span class="message-author">Richard Miles </span>
                                                    <span class="message-time">12:28 AM</span>
                                                    <div class="clearfix"></div>
                                                    <span class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/chat">
                                            <div class="list-item">
                                                <div class="list-left">
                                                    <span class="avatar">
                                                        <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-02.jpg">
                                                    </span>
                                                </div>
                                                <div class="list-body">
                                                    <span class="message-author">John Doe</span>
                                                    <span class="message-time">6 Mar</span>
                                                    <div class="clearfix"></div>
                                                    <span class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/chat">
                                            <div class="list-item">
                                                <div class="list-left">
                                                    <span class="avatar">
                                                        <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-03.jpg">
                                                    </span>
                                                </div>
                                                <div class="list-body">
                                                    <span class="message-author"> Tarah Shropshire </span>
                                                    <span class="message-time">5 Mar</span>
                                                    <div class="clearfix"></div>
                                                    <span class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/chat">
                                            <div class="list-item">
                                                <div class="list-left">
                                                    <span class="avatar">
                                                        <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-05.jpg">
                                                    </span>
                                                </div>
                                                <div class="list-body">
                                                    <span class="message-author">Mike Litorus</span>
                                                    <span class="message-time">3 Mar</span>
                                                    <div class="clearfix"></div>
                                                    <span class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="notification-message">
                                        <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/chat">
                                            <div class="list-item">
                                                <div class="list-left">
                                                    <span class="avatar">
                                                        <img alt="" src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-08.jpg">
                                                    </span>
                                                </div>
                                                <div class="list-body">
                                                    <span class="message-author"> Catherine Manseau </span>
                                                    <span class="message-time">27 Feb</span>
                                                    <div class="clearfix"></div>
                                                    <span class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="topnav-dropdown-footer">
                                <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/chat">View all Messages</a>
                            </div>
                        </div>
                    </li>
                    <!-- /Message Notifications -->

                    <li class="nav-item dropdown has-arrow main-drop">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                            <span class="user-img"><img src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/img/profiles/avatar-21.jpg" alt="">
                                <span class="status online"></span></span>
                            <span>Admin</span>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/profile">My Profile</a>
                            <a class="dropdown-item" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/settings">Settings</a>
                            <a class="dropdown-item" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/login">Logout</a>
                        </div>
                    </li>
                </ul>
                <!-- /Header Menu -->

                <!-- Mobile Menu -->
                <div class="dropdown mobile-user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/profile">My Profile</a>
                        <a class="dropdown-item" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/settings">Settings</a>
                        <a class="dropdown-item" href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/login">Logout</a>
                    </div>
                </div>
                <!-- /Mobile Menu -->

            </div>
            <!-- /Header -->

            <!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                            <li class="menu-title"> 
                                <span>Admin</span>
                            </li>
                            <li class="submenu">
                                <a href="#"><i class="la la-dashboard"></i> <span> Tasks</span> <span class="menu-arrow"></span></a>
                                <ul style="display: none;">
                                    <li><a class="active" href="{{ URL('courses') }}">Courses</a></li>
                                    <li><a href="{{ URL('students') }}">Students</a></li>
                                    <li><a href="{{ URL('lecturers') }}">Lecturers</a></li>
                                </ul>
                            </li>
                            <li class="menu-title"> 
                                <span>Employees</span>
                            </li>
                            <li class="submenu">
                                <a href="#" class="noti-dot"><i class="la la-user"></i> <span> Employees</span> <span class="menu-arrow"></span></a>
                                <ul style="display: none;">
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/employees">All Employees</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/holidays">Holidays</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/leaves">Leaves (Admin) <span class="badge badge-pill bg-primary float-right">1</span></a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/leaves-employee">Leaves (Employee)</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/leave-settings">Leave Settings</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/attendance">Attendance (Admin)</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/attendance-employee">Attendance (Employee)</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/departments">Departments</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/designations">Designations</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/timesheet">Timesheet</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/overtime">Overtime</a></li>
                                </ul>
                            </li>
                            <li> 
                                <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/clients"><i class="la la-users"></i> <span>Clients</span></a>
                            </li>
                            <li class="submenu">
                                <a href="#"><i class="la la-rocket"></i> <span> Projects</span> <span class="menu-arrow"></span></a>
                                <ul style="display: none;">
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/projects">Projects</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/tasks">Tasks</a></li>
                                    <li><a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/task-board">Task Board</a></li>
                                </ul>
                            </li>
                            <li> 
                                <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/leads"><i class="la la-user-secret"></i> <span>Leads</span></a>
                            </li>
                            <li> 
                                <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/tickets"><i class="la la-ticket"></i> <span>Tickets</span></a>
                            </li>
                            <li> 
                                <a href="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/policies"><i class="la la-file-pdf-o"></i> <span>Policies</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Sidebar -->


            @yield('content')

            <!-- /Main Wrapper -->

            <!-- jQuery -->
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/js/jquery-3.2.1.min.js"></script>

            <!-- Bootstrap Core JS -->
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/js/popper.min.js"></script>
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/js/bootstrap.min.js"></script>

            <!-- Slimscroll JS -->
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/js/jquery.slimscroll.min.js"></script>

            <!-- Chart JS -->
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/plugins/morris/morris.min.js"></script>
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/plugins/raphael/raphael.min.js"></script>
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/js/chart.js"></script>

            <!-- Custom JS -->
            <script src="https://smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/js/app.js"></script>

            <script>

$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
//    $(document).ready(function () {
//    var table = $('.leave-table').DataTable({
//        processing: true,
//        serverSide: true,
//        
//        columns: [
//            {data: 'assignedleaves', name: 'assignedleaves'},
//            {data: 'usedleaves', name: 'usedleaves'},
//            {data: 'leavebalance', name: 'leavebalance'},
//            {data: 'year', name: 'year'},
//        ]
//    });
    $('#course_btn_submit').click(function (e) {
        e.preventDefault();

//        $('#leave_Heading').html("Add Leave");
//        $('#leaveModel').modal('show');
    });


//    $('#saveleaveBtn').click(function (e) {
//        e.preventDefault();
//
//        let totalleaves = $("#totalleaves").val();
//        let year = $("#year").val();
//        let assignedleaves = $("#assignedleaves").val();
//        let _token = $("input[name=_token]").val();
//
//        $.ajax({
//            data: {
//                totalleaves: totalleaves,
//                year: year,
//                assignedleaves: assignedleaves,
//                _token: _token,
//
//            },
//            
//            type: "POST",
//            dataType: 'json',
//            success: function (data) {
//                $('#leaveModel').modal('hide');
//                table.draw();
//
//            },
//            error: function (data) {
//                console.log('Error:', data);
//            }
//        });
//    });



});

            </script>
    </body>

    <!-- Mirrored from smarthr.dreamguystech.com/smarthr-laravel/smarthr-orange/public/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Jan 2021 14:04:21 GMT -->
</html>
