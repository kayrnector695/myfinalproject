<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pagesController extends Controller {

    public function dashboard() {
        return view('master.master');
    }

    public function courses() {
        return view('Admin.courses');
    }

    public function students() {
        return view('Admin.student');
    }

    public function lecturers() {
        return view('Admin.lecture');
    }

}
